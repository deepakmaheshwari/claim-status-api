package dm.api.controller;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import dm.api.model.Claim;

@RestController
@RequestMapping("/claim-status")
public class ClaimsStatusController {

	@GetMapping(value="/{claimNumber}")
	public ResponseEntity<Claim> retrieveClaimStatus(@PathVariable("claimNumber") String claimNumber){
	
		Claim claim= new Claim();
		claim.setClaimNumber("12-1233333");
		claim.setClaimStatus("Open");
		claim.setDateOfLoss("01/15/2021");
		claim.setTimeOfLoss("04:00 PM");
		claim.setFactOfLoss("Vehicle V1 stuck V1");
		claim.setLossLocation("N Main Street, Chicago, IL");
		return ResponseEntity.ok().body(claim);
	}
	
}
