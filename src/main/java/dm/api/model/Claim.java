package dm.api.model;

public class Claim {
	private String claimNumber;
	private String factOfLoss;
	private String dateOfLoss;
	private String timeOfLoss;
	private String lossLocation;
	private String claimStatus;
	
	public String getClaimNumber() {
		return claimNumber;
	}
	public void setClaimNumber(String claimNumber) {
		this.claimNumber = claimNumber;
	}
	public String getFactOfLoss() {
		return factOfLoss;
	}
	public void setFactOfLoss(String factOfLoss) {
		this.factOfLoss = factOfLoss;
	}
	public String getDateOfLoss() {
		return dateOfLoss;
	}
	public void setDateOfLoss(String dateOfLoss) {
		this.dateOfLoss = dateOfLoss;
	}
	public String getTimeOfLoss() {
		return timeOfLoss;
	}
	public void setTimeOfLoss(String timeOfLoss) {
		this.timeOfLoss = timeOfLoss;
	}
	public String getLossLocation() {
		return lossLocation;
	}
	public void setLossLocation(String lossLocation) {
		this.lossLocation = lossLocation;
	}
	public String getClaimStatus() {
		return claimStatus;
	}
	public void setClaimStatus(String claimStatus) {
		this.claimStatus = claimStatus;
	}

}
